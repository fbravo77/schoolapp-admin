import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-configuration',
  templateUrl: './configuration.component.html',
  styleUrls: ['./configuration.component.scss']
})
export class ConfigurationComponent implements OnInit {

  cursoCard = { name: "Cursos", imgSrc: "assets/images/course-card-img.png", route: "/admin/courses", desc: "Ver - Crear - Editar cursos" }
  teacherCard = { name: "Maestros", imgSrc: "assets/images/teacher-card-img.png", route: "/admin/teachers", desc: "Ver - Crear - Editar maestros" }
  studentCard = { name: "Estudiantes", imgSrc: "assets/images/student-card-img.png", route: "/admin/students", desc: "Ver - Crear - Editar estudiandes" }
  gradesCard = { name: "Grados", imgSrc: "assets/images/grades-card.png", route: "/admin/grades", desc: "Ver - Crear - Editar grados" }
  sectionCard = { name: "Secciones", imgSrc: "assets/images/section-card.png", route: "/admin/sections", desc: "Ver - Crear - Editar secciones" }
  settingsCard = { name: "Avanzado", imgSrc: "assets/images/settings-card.png", route: "/admin/settings", desc: "Configuraciones Avanzadas" }

  constructor() { }

  ngOnInit(): void {
  }

}
