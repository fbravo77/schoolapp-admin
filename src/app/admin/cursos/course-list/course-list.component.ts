import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { faPlus, faSearch, faArrowLeft, faBars } from '@fortawesome/free-solid-svg-icons';
import { CourseService } from 'src/app/services/course.service';

@Component({
  selector: 'app-course-list',
  templateUrl: './course-list.component.html',
  styleUrls: ['./course-list.component.scss']
})
export class CourseListComponent implements OnInit {

  courseData: { id: number, name: string, year: string, active: string }[];
  resultsLength = 0;
  isLoadingResults = true;

  faPlus = faPlus;
  faSearch = faSearch;
  faLeft = faArrowLeft;
  faBars = faBars;

  courseModel: string = "";
  yearModel: string = "";
  statusModel = "-1";

  displayedColumns: string[] = ['id', 'course', 'year', 'active', 'edit'];
  dataSource = this.courseData;

  constructor(private courseService: CourseService, private location: Location) { }

  ngOnInit(): void {
    this.fetchCourses();
  }

  onPageChanged(e) {
    this.fetchCourses(e.pageIndex.toString())
  }

  fetchCourses(pageNumber = "0") {
    let status = this.statusModel == "-1" ? "" : this.statusModel;
    this.courseService.fetchCourses(this.courseModel, status, this.yearModel, pageNumber).subscribe(
      coursesResponse => {
        this.courseData = coursesResponse.body.content;
        this.resultsLength = coursesResponse.body.totalElements;
        this.isLoadingResults = false;
      },
      error => {
        this.isLoadingResults = false;
        this.courseData = [];
        console.log(error);
      }
    );
  }

  goBack(): void {
    this.location.back();
  }

}
