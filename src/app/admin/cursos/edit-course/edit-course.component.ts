import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { faPlus, faArrowLeft, } from '@fortawesome/free-solid-svg-icons';
import { NgForm } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { CourseService } from '../../../services/course.service';
import { ModalComponent } from 'src/app/general/modal/modal.component';
import { Location } from '@angular/common';

@Component({
  selector: 'app-edit-course',
  templateUrl: './edit-course.component.html',
  styleUrls: ['./edit-course.component.scss']
})
export class EditCourseComponent implements OnInit {

  @ViewChild('f', { static: true }) signupForm: NgForm;
  faPlus = faPlus;
  faLeft = faArrowLeft;
  courseModel: string;
  id: string = "";

  constructor(private courseService: CourseService,
    public dialog: MatDialog,
    private route: ActivatedRoute, private location: Location) { }

  ngOnInit(): void {
    this.getCourse();
  }

  onSubmit(form: NgForm) {
    if (form.valid) {
      this.courseService.createAndStoreCourse(this.id, form.value.course, form.value.isActive).subscribe(
        responseData => {
          if (responseData.status === 200)
            if (this.id !== "" && this.id !== null)
              this.dialog.open(ModalComponent, { data: { header: "Exito", contentModal: "Curso editado de manera exitosa", buttonText: "Ok" } });
            else
              this.dialog.open(ModalComponent, { data: { header: "Exito", contentModal: "Curso creado de manera exitosa", buttonText: "Ok" } });
        },
        error => {
          if (error.status === 0) {
            this.dialog.open(ModalComponent, { data: { header: "Error", contentModal: "Servicio no disponible, por favor contacta un administrador", buttonText: "Ok" } });
          }
          else {
            this.dialog.open(ModalComponent, { data: { header: "Error", contentModal: "Operacion fallida, por favor intenta de nuevo o contacta un administrador", buttonText: "Ok" } });
          }
        }
      );
    }
  }

  getCourse(): void {
    this.id = this.route.snapshot.paramMap.get('id');
    if (this.id !== null) {
      this.route.queryParams.subscribe(params => {
        setTimeout(() => {
          this.signupForm.setValue({
            "course": params['course'],
            "isActive": params['status'] === 'true'
          })
        });
      });
    }
  }

  goBack(): void {
    this.location.back();
  }
}
