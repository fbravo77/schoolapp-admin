import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-general-card',
  templateUrl: './general-card.component.html',
  styleUrls: ['./general-card.component.scss']
})
export class GeneralCardComponent implements OnInit {

  @Input() route: string;
  @Input() tittle: string;
  @Input() imageSrc: string;
  @Input() description: string;

  constructor() { }

  ngOnInit(): void {
  }

}
