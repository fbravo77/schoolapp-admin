import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { faPlus, faArrowLeft, } from '@fortawesome/free-solid-svg-icons';
import { NgForm } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { GradeService } from '../../../services/grade.service';
import { CourseService } from '../../../services/course.service';
import { SectionService } from '../../../services/section.service';

import { ModalComponent } from 'src/app/general/modal/modal.component';
import { Location } from '@angular/common';


@Component({
  selector: 'app-edit-grade',
  templateUrl: './edit-grade.component.html',
  styleUrls: ['./edit-grade.component.scss']
})
export class EditGradeComponent implements OnInit {
  @ViewChild('f', { static: true }) signupForm: NgForm;
  faPlus = faPlus;
  faLeft = faArrowLeft;
  gradeModel: string;
  id: string = "";

  sectionList = [];

  /* tslint:disable quotemark */
  static tube: Array<string> = [

  ];

  target = [];
  message: any;
  source = [];

  showMessage(e: any) {
    this.message = { selectChange: e };
  }

  constructor(private gradeService: GradeService,
    private courseService: CourseService,
    private sectionService: SectionService,
    public dialog: MatDialog,
    private route: ActivatedRoute,
    private location: Location) { }

  ngOnInit(): void {
    this.fetchCourses();
    this.fetchSectionList();
    this.getGrade();
  }

  fetchSectionList() {
    this.sectionService.fetchSectionList().subscribe(
      gradesResponse => {
        for (let i of gradesResponse.body) {
          this.sectionList.push({ "name": i["name"], "id": i["id"] });
        }
      },
      error => {
        console.log(error);
      }
    );
  }

  fetchCourses() {
    this.courseService.fetchActiveCourses().subscribe(
      gradesResponse => {
        for (let i of gradesResponse.body) {
          this.source.push({ "_id": i["id"], "_name": i["name"] });
        }
      },
      error => {
        console.log(error);
      }
    );
  }

  onSubmit(form: NgForm) {
    let sections = form.value.sections.join();
    let courses: string = "";
    console.log(this.target);
    if (this.message == undefined && this.target.length == 0) {
      this.dialog.open(ModalComponent, { data: { header: "Error", contentModal: "Operacion fallida, por favor ingresa cursos", buttonText: "Ok" } });
      return;
    }
    if (this.message != undefined) {
      for (let _i = 0; _i < this.message.selectChange.list.length; _i++) {
        courses += this.message.selectChange.list[_i]._id + ",";
      }
    }
    else {
      for (let _i = 0; _i < this.target.length; _i++) {
        courses += this.target[_i]._id + ",";
      }
    }

    if (form.valid) {
      this.gradeService.createAndStoreGrade(this.id, form.value.grade, form.value.isActive, courses, sections).subscribe(
        responseData => {
          if (responseData.status === 200)
            if (this.id !== "" && this.id !== null)
              this.dialog.open(ModalComponent, { data: { header: "Exito", contentModal: "Grado editado de manera exitosa", buttonText: "Ok" } });
            else
              this.dialog.open(ModalComponent, { data: { header: "Exito", contentModal: "Grado creado de manera exitosa", buttonText: "Ok" } });
        },
        error => {
          if (error.status === 0) {
            this.dialog.open(ModalComponent, { data: { header: "Error", contentModal: "Servicio no disponible, por favor contacta un administrador", buttonText: "Ok" } });
          }
          else {
            this.dialog.open(ModalComponent, { data: { header: "Error", contentModal: "Operacion fallida, por favor intenta de nuevo o contacta un administrador", buttonText: "Ok" } });
          }
        }
      );
    }
  }

  getGrade(): void {
    this.id = this.route.snapshot.paramMap.get('id');
    if (this.id !== null) {

      this.gradeService.fetchGrade(this.id).subscribe(
        gradesResponse => {
          console.log(gradesResponse);
          this.signupForm.setValue({
            "grade": gradesResponse.body.gradeName,
            "isActive": gradesResponse.body.active,
            "sections": gradesResponse.body.sections
          })
          for (let i of gradesResponse.body.courses) {
            this.target.push({ "_id": i["id"], "_name": i["courseName"] });
          }
        }, error => {
          console.log(error);
        }
      );
    }
  }

  goBack(): void {
    this.location.back();
  }
}
