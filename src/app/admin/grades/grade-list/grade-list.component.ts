import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { faPlus, faSearch, faArrowLeft, faBars } from '@fortawesome/free-solid-svg-icons';
import { GradeService } from 'src/app/services/grade.service';

@Component({
  selector: 'app-grade-list',
  templateUrl: './grade-list.component.html',
  styleUrls: ['./grade-list.component.scss']
})
export class GradeListComponent implements OnInit {
  gradeData: { id: number, gradeNumber: string, year: string, active: string }[] = [];
  resultsLength = 0;
  isLoadingResults = true;

  faPlus = faPlus;
  faSearch = faSearch;
  faLeft = faArrowLeft;
  faBars = faBars;

  gradeModel: string = "";
  yearModel: string = "";
  statusModel = "-1";

  displayedColumns: string[] = ['id', 'grade', 'year', 'active', 'edit'];
  dataSource = this.gradeData;

  constructor(private gradeService: GradeService, private location: Location) { }

  ngOnInit(): void {
    this.fetchGrades();
  }

  onPageChanged(e) {
    this.fetchGrades(e.pageIndex.toString())
  }

  fetchGrades(pageNumber = "0") {
    let status = this.statusModel == "-1" ? "" : this.statusModel;
    this.gradeService.fetchGrades(this.gradeModel, status, this.yearModel, pageNumber).subscribe(
      gradesResponse => {
        this.gradeData = gradesResponse.body.content;
        this.resultsLength = gradesResponse.body.totalElements;
        this.isLoadingResults = false;
      },
      error => {
        this.isLoadingResults = false;
        this.gradeData = [];
        console.log(error);
      }
    );
  }

  goBack(): void {
    this.location.back();
  }

}
