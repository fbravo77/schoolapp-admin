import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { faPlus, faArrowLeft, } from '@fortawesome/free-solid-svg-icons';
import { NgForm, FormControl } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { SectionService } from '../../../services/section.service';

import { ModalComponent } from 'src/app/general/modal/modal.component';
import { Location } from '@angular/common';

@Component({
  selector: 'app-edit-section',
  templateUrl: './edit-section.component.html',
  styleUrls: ['./edit-section.component.scss']
})
export class EditSectionComponent implements OnInit {

  @ViewChild('f', { static: true }) signupForm: NgForm;
  faPlus = faPlus;
  faLeft = faArrowLeft;
  sectionModel: string;
  id: string = "";

  constructor(private sectionService: SectionService,
    public dialog: MatDialog,
    private route: ActivatedRoute,
    private location: Location) { }

  ngOnInit(): void {
    this.getSection();
  }

  onSubmit(form: NgForm) {

    if (form.valid) {
      this.sectionService.createAndStoreSection(this.id, form.value.section, form.value.isActive).subscribe(
        responseData => {
          if (responseData.status === 200)
            if (this.id !== "" && this.id !== null)
              this.dialog.open(ModalComponent, { data: { header: "Exito", contentModal: "Sección editada de manera exitosa", buttonText: "Ok" } });
            else
              this.dialog.open(ModalComponent, { data: { header: "Exito", contentModal: "Sección creada de manera exitosa", buttonText: "Ok" } });
        },
        error => {
          if (error.status === 0) {
            this.dialog.open(ModalComponent, { data: { header: "Error", contentModal: "Servicio no disponible, por favor contacta un administrador", buttonText: "Ok" } });
          }
          else {
            this.dialog.open(ModalComponent, { data: { header: "Error", contentModal: "Operacion fallida, por favor intenta de nuevo o contacta un administrador", buttonText: "Ok" } });
          }
        }
      );
    }
  }

  getSection(): void {
    this.id = this.route.snapshot.paramMap.get('id');
    if (this.id !== null) {
      this.route.queryParams.subscribe(params => {
        setTimeout(() => {
          this.signupForm.setValue({
            "section": params['section'],
            "isActive": params['status'] === 'true'
          })
        });
      });
    }
  }

  goBack(): void {
    this.location.back();
  }
}
