import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { faPlus, faSearch, faArrowLeft, faBars } from '@fortawesome/free-solid-svg-icons';
import { SectionService } from 'src/app/services/section.service';

@Component({
  selector: 'app-sections-list',
  templateUrl: './sections-list.component.html',
  styleUrls: ['./sections-list.component.scss']
})
export class SectionsListComponent implements OnInit {

  sectionData: { id: number, section: string, year: string, active: string }[];
  resultsLength = 0;
  isLoadingResults = true;

  faPlus = faPlus;
  faSearch = faSearch;
  faLeft = faArrowLeft;
  faBars = faBars;

  yearModel: string = "";
  statusModel = "-1";
  sectionModel: string = "";

  displayedColumns: string[] = ['id', 'section', 'year', 'active', 'edit'];
  dataSource = this.sectionData;

  constructor(private sectionService: SectionService, private location: Location) { }

  ngOnInit(): void {
    this.fetchSections();
  }

  onPageChanged(e) {
    this.fetchSections(e.pageIndex.toString())
  }

  fetchSections(pageNumber = "0") {
    let status = this.statusModel == "-1" ? "" : this.statusModel;
    this.sectionService.fetchSections(this.sectionModel, status, this.yearModel, pageNumber).subscribe(
      gradesResponse => {
        this.sectionData = gradesResponse.body.content;
        this.resultsLength = gradesResponse.body.totalElements;
        this.isLoadingResults = false;
      },
      error => {
        this.isLoadingResults = false;
        this.sectionData = [];
        console.log(error);
      }
    );
  }

  goBack(): void {
    this.location.back();
  }

}
