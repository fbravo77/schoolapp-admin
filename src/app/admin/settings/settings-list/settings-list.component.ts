import { Component, OnInit } from '@angular/core';
import { faArrowLeft } from '@fortawesome/free-solid-svg-icons';
import { Location } from '@angular/common';

@Component({
  selector: 'app-settings-list',
  templateUrl: './settings-list.component.html',
  styleUrls: ['./settings-list.component.scss']
})
export class SettingsListComponent implements OnInit {

  faLeft = faArrowLeft;

  yearCard = { name: "Años", imgSrc: "assets/images/year-card.png", route: "/admin/years", desc: "Establecer año actual, crear o editar" }
  passwordCard = { name: "Cambiar Contraseña", imgSrc: "assets/images/password-card.png", route: "/admin/courses", desc: "Establecer año activo, crear o editar" }
  notificationCard = { name: "Enviar notificacion general", imgSrc: "assets/images/message-card.png", route: "/admin/courses", desc: "Enviar notificación general a estudiantes y encargados" }

  constructor(
    private location: Location) { }
  ngOnInit(): void {
  }

  goBack(): void {
    this.location.back();
  }

}
