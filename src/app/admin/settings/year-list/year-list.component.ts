import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { faPlus, faSearch, faBars, faArrowLeft } from '@fortawesome/free-solid-svg-icons';
import { Year } from '../../../models/year';
import { YearService } from 'src/app/services/year.service';

@Component({
  selector: 'app-year-list',
  templateUrl: './year-list.component.html',
  styleUrls: ['./year-list.component.scss']
})
export class YearListComponent implements OnInit {
  yearsData: Year[] = [];
  resultsLength = 0;
  isLoadingResults = true;

  yearModel: string = "";
  statusModel = "-1";
  faPlus = faPlus;
  faSearch = faSearch;
  faBars = faBars;
  faLeft = faArrowLeft;

  displayedColumns: string[] = ['year', 'active', 'edit'];
  dataSource = this.yearsData;
  constructor(private yearService: YearService, private location: Location) {
  }

  onPageChanged(e) {
    this.fetchYears(e.pageIndex.toString())
  }
  ngOnInit(): void {
    this.fetchYears();
  }

  fetchYears(pageNumber = "0") {
    let status = this.statusModel == "-1" ? "" : this.statusModel;
    this.yearService.fetchYears(this.yearModel, status, pageNumber).subscribe(
      yearsResponse => {
        this.yearsData = yearsResponse.body.content;
        this.resultsLength = yearsResponse.body.totalElements;
        this.isLoadingResults = false;
      },
      error => {
        this.isLoadingResults = false;
        this.yearsData = [];
        console.log(error);
      }
    );
  }

  goBack(): void {
    this.location.back();
  }
}
