import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { faPlus, faArrowLeft, } from '@fortawesome/free-solid-svg-icons';
import { NgForm } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { YearService } from '../../../services/year.service';
import { ModalComponent } from 'src/app/general/modal/modal.component';
import { Location } from '@angular/common';

@Component({
  selector: 'app-year-new',
  templateUrl: './year-new.component.html',
  styleUrls: ['./year-new.component.scss']
})
export class YearNewComponent implements OnInit {
  @ViewChild('f', { static: true }) signupForm: NgForm;
  faPlus = faPlus;
  faLeft = faArrowLeft;
  yearModel = 0;
  id: string = "";

  constructor(
    private yearService: YearService,
    public dialog: MatDialog,
    private route: ActivatedRoute,
    private router: Router,
    private location: Location) { }

  ngOnInit(): void {
    this.getYear();
  }

  getYear(): void {
    this.id = this.route.snapshot.paramMap.get('id');
    console.log(this.id);
    if (this.id !== null) {
      this.route.queryParams.subscribe(params => {
        setTimeout(() => {
          this.signupForm.setValue({
            "year": params['year'],
            "isActive": params['status'] === 'true'
          })
        });

      });
    }
  }

  goBack(): void {
    this.location.back();
  }

  onSubmit(form: NgForm) {
    if (form.valid) {
      this.yearService.createAndStoreYear(this.id, form.value.year, form.value.isActive).subscribe(
        responseData => {
          if (responseData.status === 200)
            if (this.id !== "" && this.id !== null)
              this.dialog.open(ModalComponent, { data: { header: "Exito", contentModal: "Año editado de manera exitosa", buttonText: "Ok" } });
            else
              this.dialog.open(ModalComponent, { data: { header: "Exito", contentModal: "Año creado de manera exitosa", buttonText: "Ok" } });

          this.router.navigate(['/admin/years']);
        },
        error => {
          if (error.status === 0) {
            this.dialog.open(ModalComponent, { data: { header: "Error", contentModal: "Servicio no disponible, por favor contacta un administrador", buttonText: "Ok" } });
          }
          else {
            this.dialog.open(ModalComponent, { data: { header: "Error", contentModal: "Operacion fallida, por favor intenta de nuevo o contacta un administrador", buttonText: "Ok" } });
          }
        }
      );
    }
  }
}
