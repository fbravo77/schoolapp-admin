import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { faPlus, faArrowLeft, faTrash, } from '@fortawesome/free-solid-svg-icons';
import { NgForm } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { GradeService } from '../../../services/grade.service';
import { SectionService } from '../../../services/section.service';
import { StudentService } from '../../../services/student.service';

import { Location } from '@angular/common';
import { MatTable } from '@angular/material/table';
import { ModalComponent } from 'src/app/general/modal/modal.component';

@Component({
  selector: 'app-edit-student',
  templateUrl: './edit-student.component.html',
  styleUrls: ['./edit-student.component.scss']
})
export class EditStudentComponent implements OnInit {

  @ViewChild('f', { static: true }) signupForm: NgForm;
  @ViewChild(MatTable) table: MatTable<any>;

  /**TEACHER DATA */
  faPlus = faPlus;
  faLeft = faArrowLeft;
  faTrash = faTrash;

  firstNameModel: string;
  lastNameModel: string;
  personChargeModel: string;
  personChargeModel2: string;
  contactPhoneModel: string;
  contactPhone2Model: string;
  contactPhone3Model: string;
  alergiesModel: string;
  genderModel: string;
  dateBirthModel: string;
  emailModel: string;
  isActive: string;
  id: string = "";
  minDate: Date;
  maxDate: Date;

  //GRADOS
  gradeList = [];
  selectedGrade: string;

  //SECCIONES
  sectionList = [];
  selectedSection: string;

  constructor(private gradeService: GradeService,
    private studentService: StudentService,
    private sectionService: SectionService,
    public dialog: MatDialog,
    private route: ActivatedRoute,
    private location: Location) {
    const currentYear = new Date().getFullYear();
    this.minDate = new Date(currentYear - 80, 0, 1);
    this.maxDate = new Date(currentYear - 18, 12, 31);
  }

  ngOnInit(): void {
    this.fetchGradesList();
  }

  fetchGradesList() {
    this.gradeService.fetchGradesList().subscribe(
      gradesResponse => {

        for (let i of gradesResponse.body) {
          this.gradeList.push({ "gradeName": i["gradeName"], "id": i["id"] });
        }
      },
      error => {
        console.log(error);
      }
    );
  }

  select(event: any) {
    this.selectedSection = "";
    this.selectedGrade = event;
    this.fetchSectionsByGrade(event);
  }

  selectSections(event: any) {
    this.selectedSection = event;
  }

  fetchSectionsByGrade(gradeId: string) {
    this.sectionService.fetchSectionsByGrade(gradeId).subscribe(
      gradesResponse => {
        this.sectionList = []
        for (let i of gradesResponse.body) {
          this.sectionList.push({ "name": i["name"], "id": i["id"] });
        }
      },
      error => {
        console.log(error);
      }
    );
  }

  onSubmit(form: NgForm) {

    if (form.valid) {
      let gradeSection = this.selectedGrade + "," + this.selectedSection;
      this.studentService.createAndStoreStudent(form.value.firstName, form.value.lastName,
        form.value.personCharge, form.value.otherPersonCharge,
        form.value.contactPhone,
        form.value.genderModel, this.dateBirthModel, form.value.email, form.value.isActive, form.value.contactPhone2
        , form.value.contactPhone3, form.value.alergies, gradeSection).subscribe(
          responseData => {
            if (responseData.status === 200)
              if (this.id !== "" && this.id !== null)
                this.dialog.open(ModalComponent, { data: { header: "Exito", contentModal: "Grado editado de manera exitosa", buttonText: "Ok" } });
              else
                this.dialog.open(ModalComponent, { data: { header: "Exito", contentModal: "Grado creado de manera exitosa", buttonText: "Ok" } });
          },
          error => {
            if (error.status === 0) {
              this.dialog.open(ModalComponent, { data: { header: "Error", contentModal: "Servicio no disponible, por favor contacta un administrador", buttonText: "Ok" } });
            }
            else {
              this.dialog.open(ModalComponent, { data: { header: "Error", contentModal: "Operacion fallida, por favor intenta de nuevo o contacta un administrador", buttonText: "Ok" } });
            }
          }
        );

    }
  }
  goBack(): void {
    this.location.back();
  }
}
