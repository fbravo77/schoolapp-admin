import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { faPlus, faSearch, faArrowLeft } from '@fortawesome/free-solid-svg-icons';
import { StudentService } from 'src/app/services/student.service';

@Component({
  selector: 'app-students-list',
  templateUrl: './students-list.component.html',
  styleUrls: ['./students-list.component.scss']
})
export class StudentsListComponent implements OnInit {

  teacherData: { firstName: string, lastName: string, contactPhone1: string, active: string }[];
  resultsLength = 0;
  isLoadingResults = true;

  faPlus = faPlus;
  faSearch = faSearch;
  faLeft = faArrowLeft;

  firstNameModel: string = "";
  lastNameModel: string = "";
  yearModel: string = "";
  statusModel = "-1";
  genderModel = "-1";
  mailModel: string = ""
  displayedColumns: string[] = ['id', 'firstName', 'lastName', 'active'];
  dataSource = this.teacherData;

  constructor(private studentService: StudentService, private location: Location) { }

  ngOnInit(): void {
    this.fetchStudents();
  }

  onPageChanged(e) {
    this.fetchStudents(e.pageIndex.toString())
  }

  fetchStudents(pageNumber = "0") {
    let status = this.statusModel == "-1" ? "" : this.statusModel;
    let gender = this.genderModel == "-1" ? "" : this.genderModel;
    this.studentService.fetchStudent(this.firstNameModel, this.lastNameModel, gender, status, this.yearModel, pageNumber).subscribe(
      teacherResponse => {
        this.teacherData = teacherResponse.body.content;
        this.resultsLength = teacherResponse.body.totalElements;
        this.isLoadingResults = false;
      },
      error => {
        this.isLoadingResults = false;
        this.teacherData = [];
        console.log(error);
      }
    );
  }

  goBack(): void {
    this.location.back();
  }
}
