import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { faPlus, faArrowLeft, faTrash, } from '@fortawesome/free-solid-svg-icons';
import { NgForm } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { GradeService } from '../../../services/grade.service';
import { TeacherService } from '../../../services/teacher.service';
import { SectionService } from '../../../services/section.service';

import { ModalComponent } from 'src/app/general/modal/modal.component';
import { Location } from '@angular/common';
import { MatTable } from '@angular/material/table';

@Component({
  selector: 'app-edit-teacher',
  templateUrl: './edit-teacher.component.html',
  styleUrls: ['./edit-teacher.component.scss']
})
export class EditTeacherComponent implements OnInit {

  @ViewChild('f', { static: true }) signupForm: NgForm;
  @ViewChild(MatTable) table: MatTable<any>;

  /**TEACHER DATA */
  faPlus = faPlus;
  faLeft = faArrowLeft;
  faTrash = faTrash;

  firstNameModel: string;
  lastNameModel: string;
  contactPhoneModel: string;
  contactPhone2Model: string;
  contactPhone3Model: string;
  alergiesModel: string;
  genderModel: string;
  dateBirthModel: string;
  emailModel: string;
  isActive: string;
  id: string = "";
  minDate: Date;
  maxDate: Date;


  //CURSOS
  targetCourse = [];
  courseList = [];
  selectedCourses: [];

  //GRADOS
  gradeList = [];
  selectedGrade: string;

  //SECCIONES
  sectionList = [];
  selectedSections: [];

  //TABLE TEACHER
  displayedColumns: string[] = ['id', 'grade', 'section', 'course', 'delete'];
  teacherData: { grade: string, section: string, course: string }[] = [];
  dataSource = this.teacherData;

  constructor(private gradeService: GradeService,
    private teacherService: TeacherService,
    private sectionService: SectionService,
    public dialog: MatDialog,
    private route: ActivatedRoute,
    private location: Location) {

    const currentYear = new Date().getFullYear();
    this.minDate = new Date(currentYear - 80, 0, 1);
    this.maxDate = new Date(currentYear - 18, 12, 31);
  }

  ngOnInit(): void {
    this.fetchGradesList();
  }

  fetchGradesList() {
    this.gradeService.fetchGradesList().subscribe(
      gradesResponse => {

        for (let i of gradesResponse.body) {
          this.gradeList.push({ "gradeName": i["gradeName"], "id": i["id"] });
        }
      },
      error => {
        console.log(error);
      }
    );
  }

  select(event: any) {
    this.selectedSections = [];
    this.selectedGrade = event;
    this.fetchSectionsByGrade(event);
    this.fetchCoursesByGrade(event);
  }

  selectSections(event: any) {
    this.selectedSections = event;
  }

  selectCourses(event: any) {
    this.selectedCourses = event;
  }

  fetchCoursesByGrade(gradeId: string) {
    this.gradeService.fetchGrade(gradeId).subscribe(
      gradesResponse => {
        this.courseList = []
        if (gradesResponse.body.courses != null) {
          this.courseList = [];
          for (let i of gradesResponse.body.courses) {
            this.courseList.push({ "name": i["courseName"], "id": i["id"] });
          }
        }
      },
      error => {
        console.log(error);
      }
    );
  }

  fetchSectionsByGrade(gradeId: string) {
    this.sectionService.fetchSectionsByGrade(gradeId).subscribe(
      gradesResponse => {
        this.sectionList = []
        console.log("secciones: ", gradesResponse.body)
        for (let i of gradesResponse.body) {
          this.sectionList.push({ "name": i["name"], "id": i["id"] });
        }
      },
      error => {
        console.log(error);
      }
    );
  }

  onSubmit(form: NgForm) {

    if (form.valid) {

      this.teacherService.createAndStoreTeacher(form.value.firstName, form.value.lastName, form.value.contactPhone,
        form.value.genderModel, this.dateBirthModel, form.value.email, form.value.isActive, form.value.contactPhone2
        , form.value.contactPhone3, form.value.alergies, this.targetCourse).subscribe(
          responseData => {
            if (responseData.status === 200)
              if (this.id !== "" && this.id !== null)
                this.dialog.open(ModalComponent, { data: { header: "Exito", contentModal: "Grado editado de manera exitosa", buttonText: "Ok" } });
              else
                this.dialog.open(ModalComponent, { data: { header: "Exito", contentModal: "Grado creado de manera exitosa", buttonText: "Ok" } });
          },
          error => {
            if (error.status === 0) {
              this.dialog.open(ModalComponent, { data: { header: "Error", contentModal: "Servicio no disponible, por favor contacta un administrador", buttonText: "Ok" } });
            }
            else {
              this.dialog.open(ModalComponent, { data: { header: "Error", contentModal: "Operacion fallida, por favor intenta de nuevo o contacta un administrador", buttonText: "Ok" } });
            }
          }
        );
    }
  }
  goBack(): void {
    this.location.back();
  }

  addCourse(): void {
    if (this.selectedGrade && this.selectedSections && this.selectedCourses) {
      let currentGrade = this.gradeList.find(el => el.id === this.selectedGrade)
      for (var section of this.selectedSections) {
        let currentSection = this.sectionList.find(el => el.id === section)
        for (var course of this.selectedCourses) {
          let currentCourse = this.courseList.find(el => el.id === course)
          let data = { grade: currentGrade.gradeName, section: currentSection.name, course: currentCourse.name };
          if (!this.teacherData.some(el => el.grade == data.grade && el.section == data.section && el.course == data.course)) {
            this.teacherData.push(data)
            this.targetCourse.push({ gradeId: this.selectedGrade, sectionId: section, courseId: course })
          }
          else {
            console.log("CURSO ya Existe");
          }
        }
      }
      this.table.renderRows();
    }
    else {
      console.log("Agregar cursos");
    }
  }

  removeAllCourses(): void {
    this.teacherData = [];
    this.targetCourse = [];
  }

  deleteCourse(rowId: number): void {
    this.teacherData.splice(rowId, 1);
    this.targetCourse.splice(rowId, 1);
    this.table.renderRows();
  }
}
