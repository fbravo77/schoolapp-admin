import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { faPlus, faSearch, faArrowLeft } from '@fortawesome/free-solid-svg-icons';
import { TeacherService } from 'src/app/services/teacher.service';

@Component({
  selector: 'app-teachers-list',
  templateUrl: './teachers-list.component.html',
  styleUrls: ['./teachers-list.component.scss']
})
export class TeachersListComponent implements OnInit {

  teacherData: { firstName: string, lastName: string, contactPhone1: string, active: string }[];
  resultsLength = 0;
  isLoadingResults = true;

  faPlus = faPlus;
  faSearch = faSearch;
  faLeft = faArrowLeft;

  firstNameModel: string = "";
  lastNameModel: string = "";
  yearModel: string = "";
  statusModel = "-1";
  genderModel = "-1";
  mailModel: string = ""
  displayedColumns: string[] = ['id', 'firstName', 'lastName', 'contactPhone1', 'active'];
  dataSource = this.teacherData;

  constructor(private teacherService: TeacherService, private location: Location) { }

  ngOnInit(): void {
    this.fetchTeacher();
  }

  onPageChanged(e) {
    this.fetchTeacher(e.pageIndex.toString())
  }

  fetchTeacher(pageNumber = "0") {
    let status = this.statusModel == "-1" ? "" : this.statusModel;
    let gender = this.genderModel == "-1" ? "" : this.genderModel;
    this.teacherService.fetchTeachers(this.firstNameModel, this.lastNameModel, gender, status, this.yearModel, pageNumber).subscribe(
      teacherResponse => {
        this.teacherData = teacherResponse.body.content;
        this.resultsLength = teacherResponse.body.totalElements;
        this.isLoadingResults = false;
      },
      error => {
        this.isLoadingResults = false;
        this.teacherData = [];
        console.log(error);
      }
    );
  }

  goBack(): void {
    this.location.back();
  }

}
