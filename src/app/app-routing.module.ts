import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ConfigurationComponent } from './admin/configuration/configuration.component';
import { MainComponent } from './admin/main/main.component';
import { PageNotFoundComponent } from './general/page-not-found/page-not-found.component';
import { StudentsListComponent } from './admin/students/students-list/students-list.component';
import { TeachersListComponent } from './admin/teachers/teachers-list/teachers-list.component';
import { CourseListComponent } from './admin/cursos/course-list/course-list.component';
import { SettingsListComponent } from './admin/settings/settings-list/settings-list.component';
import { YearListComponent } from './admin/settings/year-list/year-list.component';
import { YearNewComponent } from './admin/settings/year-new/year-new.component';
import { GradeListComponent } from './admin/grades/grade-list/grade-list.component';
import { EditGradeComponent } from './admin/grades/edit-grade/edit-grade.component';
import { SectionsListComponent } from './admin/sections/sections-list/sections-list.component';
import { EditCourseComponent } from './admin/cursos/edit-course/edit-course.component';
import { EditSectionComponent } from './admin/sections/edit-section/edit-section.component';
import { EditTeacherComponent } from './admin/teachers/edit-teacher/edit-teacher.component';
import { EditStudentComponent } from './admin/students/edit-student/edit-student.component';
import { IndexComponent } from './index/index.component';
import { LoginComponent } from './login/login.component';

import { ConfigurationComponent as confTeacherComponent } from './teacher-admin/configuration/configuration.component';
import { TeacherCoursesComponent } from './teacher-admin/teacher-courses/teacher-courses.component';

import { AuthorizationGuardService as guard } from './guards/authorization-guard';
import { CourseDetailComponent } from './teacher-admin/course-detail/course-detail.component';
import { NewHomeworkComponent } from './teacher-admin/new-homework/new-homework.component';
import { CheckHomeworkComponent } from './teacher-admin/check-homework/check-homework.component';

const appRoutes: Routes = [
    { path: '', component: IndexComponent },
    { path: 'login', component: LoginComponent },
    //ADMIN
    {
        path: 'admin',
        component: MainComponent,
        children: [
            { path: 'config', component: ConfigurationComponent, canActivate: [guard], data: { expectedRol: ['admin', 'coordinator'] } },
            { path: 'settings', component: SettingsListComponent, canActivate: [guard], data: { expectedRol: ['admin', 'coordinator'] } },

            //STUDENTS
            {
                path: 'students',
                children: [
                    { path: '', component: StudentsListComponent, canActivate: [guard], data: { expectedRol: ['admin', 'coordinator'] } },
                    { path: 'new', component: EditStudentComponent, canActivate: [guard], data: { expectedRol: ['admin', 'coordinator'] } },
                    { path: 'edit/:id', component: EditStudentComponent, canActivate: [guard], data: { expectedRol: ['admin', 'coordinator'] } },
                ]
            },

            //TEACHERS
            {
                path: 'teachers',
                children: [
                    { path: '', component: TeachersListComponent, canActivate: [guard], data: { expectedRol: ['admin', 'coordinator'] } },
                    { path: 'new', component: EditTeacherComponent, canActivate: [guard], data: { expectedRol: ['admin', 'coordinator'] } },
                    { path: 'edit/:id', component: EditTeacherComponent, canActivate: [guard], data: { expectedRol: ['admin', 'coordinator'] } },
                ]
            },

            //COURSES
            {
                path: 'courses',
                children: [
                    { path: '', component: CourseListComponent, canActivate: [guard], data: { expectedRol: ['admin', 'coordinator'] } },
                    { path: 'new', component: EditCourseComponent, canActivate: [guard], data: { expectedRol: ['admin', 'coordinator'] } },
                    { path: 'edit/:id', component: EditCourseComponent, canActivate: [guard], data: { expectedRol: ['admin', 'coordinator'] } },
                ]
            },

            //GRADES
            {
                path: 'grades',
                children: [
                    { path: '', component: GradeListComponent, canActivate: [guard], data: { expectedRol: ['admin', 'coordinator'] } },
                    { path: 'new', component: EditGradeComponent, canActivate: [guard], data: { expectedRol: ['admin', 'coordinator'] } },
                    { path: 'edit/:id', component: EditGradeComponent, canActivate: [guard], data: { expectedRol: ['admin', 'coordinator'] } },
                ]
            },
            //SECTIONS
            {
                path: 'sections',
                children: [
                    { path: '', component: SectionsListComponent, canActivate: [guard], data: { expectedRol: ['admin', 'coordinator'] } },
                    { path: 'new', component: EditSectionComponent, canActivate: [guard], data: { expectedRol: ['admin', 'coordinator'] } },
                    { path: 'edit/:id', component: EditSectionComponent, canActivate: [guard], data: { expectedRol: ['admin', 'coordinator'] } },
                ]
            },
            //YEARS
            { path: 'years', component: YearListComponent, canActivate: [guard], data: { expectedRol: ['admin', 'coordinator'] } },
            { path: 'years/new', component: YearNewComponent, canActivate: [guard], data: { expectedRol: ['admin'] } },
            { path: 'years/edit/:id', component: YearNewComponent, canActivate: [guard], data: { expectedRol: ['admin'] }, },
            //{ path: ':id/edit', component: EditServerComponent, canDeactivate: [CanDeactivateGuard] }
        ]
    },
    //ADMIN-TEACHER
    {
        path: 'admin-teacher',
        component: MainComponent, canActivate: [guard], data: {
            expectedRol: ['teacher']
        },
        children: [
            { path: 'config', component: confTeacherComponent },
            { path: 'courses', component: TeacherCoursesComponent },
            { path: 'course-detail/:id', component: CourseDetailComponent },
            { path: 'new-homework/:courseId', component: NewHomeworkComponent },
            { path: 'check-homework/:homeworkId', component: CheckHomeworkComponent },
        ],
    },
    //GENERAL
    { path: 'not-found', component: PageNotFoundComponent },
    { path: '**', redirectTo: '/not-found' }
];

@NgModule({
    imports: [
        // RouterModule.forRoot(appRoutes, {useHash: true})
        RouterModule.forRoot(appRoutes)
    ],
    exports: [RouterModule]
})
export class AppRoutingModule {

}