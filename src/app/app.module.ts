import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { HttpClientModule } from '@angular/common/http';

import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatCardModule } from '@angular/material/card';
import { MatButtonModule } from '@angular/material/button';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material/core';
import { MatSelectModule } from '@angular/material/select';
import { MatDialogModule } from '@angular/material/dialog';
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatMenuModule } from '@angular/material/menu';


import { AngularDualListBoxModule } from 'angular-dual-listbox';
import { CustomListboxModule } from './general/custom-dual-list/custom-listbox.module';



import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavbarComponent } from './general/navbar/navbar.component';
import { ModalComponent } from './general/modal/modal.component';
import { LogosComponent } from './general/logos/logos.component';
import { ConfigurationComponent } from './admin/configuration/configuration.component';
import { StudentsListComponent } from './admin/students/students-list/students-list.component';
import { MainComponent } from './admin/main/main.component';
import { PageNotFoundComponent } from './general/page-not-found/page-not-found.component';
import { RibbonComponent } from './general/ribbon/ribbon.component';
import { TeachersListComponent } from './admin/teachers/teachers-list/teachers-list.component';
import { CourseListComponent } from './admin/cursos/course-list/course-list.component';
import { SettingsListComponent } from './admin/settings/settings-list/settings-list.component';
import { GeneralCardComponent } from './admin/general-card/general-card.component';
import { YearListComponent } from './admin/settings/year-list/year-list.component';
import { YearNewComponent } from './admin/settings/year-new/year-new.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { GradeListComponent } from './admin/grades/grade-list/grade-list.component';
import { EditGradeComponent } from './admin/grades/edit-grade/edit-grade.component';
import { SectionsListComponent } from './admin/sections/sections-list/sections-list.component';
import { EditCourseComponent } from './admin/cursos/edit-course/edit-course.component';
import { EditSectionComponent } from './admin/sections/edit-section/edit-section.component';
import { EditTeacherComponent } from './admin/teachers/edit-teacher/edit-teacher.component';
import { EditStudentComponent } from './admin/students/edit-student/edit-student.component';
import { IndexComponent } from './index/index.component';
import { LoginComponent } from './login/login.component';
import { interceptorProvider } from './interceptors/auth-interceptor.service';
import { ConfigurationComponent as confTeacherComponent } from './teacher-admin/configuration/configuration.component';
import { TeacherCoursesComponent } from './teacher-admin/teacher-courses/teacher-courses.component';
import { CourseDetailComponent } from './teacher-admin/course-detail/course-detail.component';
import { NewHomeworkComponent } from './teacher-admin/new-homework/new-homework.component';
import { CheckHomeworkComponent } from './teacher-admin/check-homework/check-homework.component';
import { NewMessageComponent } from './teacher-admin/new-message/new-message.component';

//import { CustomDualListComponent } from './general/custom-dual-list/custom-dual-list.component';
import { AngularEditorModule } from '@kolkov/angular-editor';


@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    ModalComponent,
    LogosComponent,
    ConfigurationComponent,
    StudentsListComponent,
    MainComponent,
    PageNotFoundComponent,
    RibbonComponent,
    TeachersListComponent,
    CourseListComponent,
    SettingsListComponent,
    GeneralCardComponent,
    YearListComponent,
    YearNewComponent,
    GradeListComponent,
    EditGradeComponent,
    SectionsListComponent,
    EditCourseComponent,
    EditSectionComponent,
    EditTeacherComponent,
    EditStudentComponent,
    IndexComponent,
    LoginComponent,
    confTeacherComponent,
    TeacherCoursesComponent,
    CourseDetailComponent,
    NewHomeworkComponent,
    CheckHomeworkComponent,
    NewMessageComponent,
    //CustomDualListComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule,
    FontAwesomeModule,
    HttpClientModule,
    MatFormFieldModule,
    BrowserAnimationsModule,
    MatInputModule,
    MatCardModule,
    MatButtonModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatSelectModule,
    MatDialogModule,
    MatTableModule,
    MatPaginatorModule,
    MatProgressSpinnerModule,
    MatAutocompleteModule,
    AngularDualListBoxModule,
    CustomListboxModule,
    MatMenuModule, AngularEditorModule
  ],
  providers: [interceptorProvider],
  bootstrap: [AppComponent]
})
export class AppModule { }
