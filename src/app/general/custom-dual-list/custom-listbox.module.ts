import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { MatButtonModule } from '@angular/material/button';


import { CustomDualListComponent } from './custom-dual-list.component';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        FontAwesomeModule,
        MatButtonModule
    ],
    declarations: [
        CustomDualListComponent
    ],
    exports: [
        CustomDualListComponent
    ]
})
export class CustomListboxModule { }