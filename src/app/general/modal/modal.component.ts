import { Component, OnInit, Input, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Router } from '@angular/router';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.scss']
})
export class ModalComponent implements OnInit {

  @Input() header: string;
  @Input() contentModal: string;
  @Input() buttonText: string;
  @Input() navigateTo: string;
  @Input() paramsRouter: string;

  constructor(@Inject(MAT_DIALOG_DATA)
  public data: { header: String, contentModal: String, buttonText: String }, private router: Router) { }


  ngOnInit(): void {

  }

  onCloseModal() {
    if (this.navigateTo != null && this.navigateTo != "") {
      this.router.navigate(['/heroes']);
    }
  }



}
