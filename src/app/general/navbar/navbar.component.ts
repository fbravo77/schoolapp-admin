import { Component, OnInit } from '@angular/core';
import { TokenService } from 'src/app/services/token.service';
import { faHome, faPowerOff } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {

  faHome = faHome;
  faPowerOff = faPowerOff;

  constructor(private tokenService: TokenService) { }

  ngOnInit(): void {
  }

  logout(): void {
    this.tokenService.logOut();
  }

}
