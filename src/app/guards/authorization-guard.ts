import { Injectable } from '@angular/core';
import { TokenService } from '../services/token.service';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';

@Injectable({
    providedIn: 'root'
})
export class AuthorizationGuardService implements CanActivate {

    realRol: string;

    constructor(private tokenService: TokenService, private router: Router) { }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
        const expectedRol = route.data.expectedRol;

        const roles = this.tokenService.getAuthorities();

        this.realRol = "";

        roles.forEach(rol => {
            if (rol === "ROLE_ADMIN") {
                this.realRol = "admin";
            }
            else if (rol === "ROLE_COORDINATOR") {
                this.realRol = "coordinator";
            }
            else if (rol === "ROLE_TEACHER") {
                this.realRol = "teacher";
            }
        });
        if (!this.tokenService.getToken() || expectedRol.indexOf(this.realRol) === -1) {
            this.router.navigate(["/"]);
            return false;
        }
        return true;
    }
}