import { Component, OnInit } from '@angular/core';
import { TokenService } from '../services/token.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.scss']
})
export class IndexComponent implements OnInit {

  currentRole: string

  constructor(private tokenService: TokenService, private router: Router) { }

  ngOnInit(): void {
    if (!this.tokenService.getToken()) {
      this.router.navigate(["/login"]);
    }
    else {
      const roles = this.tokenService.getAuthorities();
      roles.forEach(rol => {
        if (rol === "ROLE_ADMIN" || rol === "ROLE_COORDINATOR") {
          this.router.navigate(["/admin/config"]);
        }
        else if (rol === "ROLE_TEACHER") {
          console.log("isTheacher");
          this.router.navigate(["/admin-teacher/config"]);
        }
      });
    }
  }

}
