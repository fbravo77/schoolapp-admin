import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpEvent, HttpHandler, HttpRequest, HTTP_INTERCEPTORS } from '@angular/common/http';
import { Observable } from 'rxjs';
import { TokenService } from '../services/token.service';

@Injectable({
    providedIn: 'root'
})
export class AuthInterceptorService implements HttpInterceptor {

    constructor(private tokenService: TokenService) { }

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        let tokenReq = req;
        const token = this.tokenService.getToken();
        if (token != null) {
            tokenReq = req.clone({ headers: req.headers.set("Authorization", "Bearer " + token) })
            return next.handle(tokenReq);
        }
        else
            return next.handle(req);
    }
}

export const interceptorProvider = [{ provide: HTTP_INTERCEPTORS, useClass: AuthInterceptorService, multi: true }];