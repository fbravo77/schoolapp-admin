import { Component, OnInit } from '@angular/core';
import { AuthService } from '../services/auth.service';
import { TokenService } from '../services/token.service';
import { Router } from '@angular/router';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  errMessage: string;
  isLogged = false;
  isLoginFail = false;
  userName: string;
  password: string;
  roles: string[] = [];

  constructor(private tokenService: TokenService,
    private authService: AuthService,
    private router: Router
  ) { }

  ngOnInit(): void {
    if (this.tokenService.getToken()) {
      this.isLogged = true;
      this.isLoginFail = false;
      this.roles = this.tokenService.getAuthorities();
    }
  }

  onLogin(form: NgForm): void {
    if (form.valid) {
      this.authService.login(this.userName, this.password).subscribe(
        data => {
          console.log(data);
          if (data.authorities) {
            for (let dat of data.authorities) {
              if (dat.authority === "ROLE_STUDENT") {
                this.isLogged = false;
                this.isLoginFail = true;
                this.errMessage = "No tienes permiso para acceder al sistema, por favor contacta al administrador";
                return;
              }
            }
          }

          this.isLogged = true;
          this.isLoginFail = false;
          this.tokenService.setToken(data.token);
          this.tokenService.setUserName(data.userName);
          this.tokenService.setAuthorities(data.authorities);
          this.roles = data.authorities;
          this.router.navigate(['/admin/config'])
        },
        error => {
          this.isLogged = false;
          this.isLoginFail = true;
          this.errMessage = error.message;
          if (error.status === 0)
            this.errMessage = "No se pudo establecer conexión con servidor, por favor intentar mas tarde o contacte administrador";
          if (error.status === 404)
            this.errMessage = "Credenciales invalidas, por favor revisa tu usuario o contraseña";
        }
      )
    }
    else {
      this.isLoginFail = true;
      this.errMessage = "Por favor llena los campos";
    }
  }

  getPassword(): void {
    console.log("CHANGE PASSWORD");
  }
}
