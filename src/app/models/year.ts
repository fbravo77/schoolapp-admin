export interface Year {
    id: number;
    year: number;
    active: boolean;
}