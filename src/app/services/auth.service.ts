import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';

const TOKEN_KEY = "AuthToken";
const USERNAME_KEY = "AuthUserName";
const AUTHORITI_KEY = "AuthAuthorities";

@Injectable({
    providedIn: 'root'
})
export class AuthService {

    constructor(private http: HttpClient) { }

    loginUrl = environment.serverURL + "/auth/login";
    //loginUrl = 'http://SchoolBackend-env.eba-sebmzjkh.us-east-1.elasticbeanstalk.com/auth/login';
    login(userName: string, password: string) {
        const postData = { userName: userName, password: password };
        return this.http
            .post<any>(this.loginUrl, postData);
    }
}