import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Subject, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { environment } from '../../environments/environment';

@Injectable({
    providedIn: 'root'
})
export class CourseService {

    constructor(private http: HttpClient) { }

    createAndStoreCourse(id: string, name: string, active: boolean) {
        const postData = { id: id, name: name, active: active };
        return this.http
            .post(
                environment.serverURL + '/course',
                postData,
                {
                    observe: 'response'
                }
            ).pipe(catchError(errorRes => {
                return throwError(errorRes);
            }));

    }

    fetchActiveCourses() {

        return this.http
            .get<any>(
                environment.serverURL + '/course/all',
                {
                    //   headers: new HttpHeaders({ 'Custom-Header': 'Hello' }),
                    responseType: 'json',
                    observe: 'response'
                }
            )
            .pipe(
                catchError(errorRes => {
                    return throwError(errorRes);
                })
            );
    }

    fetchCourses(course: string, active: string, year: string, page: string) {
        let searchParams = new HttpParams();
        searchParams = searchParams.append('page', page);
        searchParams = searchParams.append('name', course);
        searchParams = searchParams.append('active', active);
        searchParams = searchParams.append('year', year);
        return this.http
            .get<any>(
                environment.serverURL + '/course',
                {
                    params: searchParams,
                    responseType: 'json',
                    observe: 'response'
                }
            )
            .pipe(
                catchError(errorRes => {
                    return throwError(errorRes);
                })
            );
    }

    fetchTeacherCourses(course: string, page: string) {
        let searchParams = new HttpParams();
        searchParams = searchParams.append('page', page);
        searchParams = searchParams.append('name', course);
        return this.http
            .get<any>(
                environment.serverURL + '/course/teacher',
                {
                    params: searchParams,
                    responseType: 'json',
                    observe: 'response'
                }
            )
            .pipe(
                catchError(errorRes => {
                    return throwError(errorRes);
                })
            );
    }

    fetchTeacherCourseById(courseId: string) {
        let searchParams = new HttpParams();
        searchParams = searchParams.append('id', courseId);
        return this.http
            .get<any>(
                environment.serverURL + '/course/teacher-course',
                {
                    params: searchParams,
                    responseType: 'json',
                    observe: 'response'
                }
            )
            .pipe(
                catchError(errorRes => {
                    return throwError(errorRes);
                })
            );
    }
}