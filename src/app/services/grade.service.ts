import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { environment } from '../../environments/environment';

@Injectable({
    providedIn: 'root'
})
export class GradeService {

    constructor(private http: HttpClient) { }

    createAndStoreGrade(id: string, name: string, active: boolean, courses: string, sections: string) {
        const postData = { id: id, name: name, active: active, courses: courses, sections: sections };
        return this.http
            .post(
                environment.serverURL + '/grade',
                postData,
                {
                    observe: 'response'
                }
            ).pipe(catchError(errorRes => {
                return throwError(errorRes);
            }));

    }

    fetchGradesList() {
        return this.http
            .get<any>(
                environment.serverURL + '/grade/all',
                {
                    //   headers: new HttpHeaders({ 'Custom-Header': 'Hello' }),
                    responseType: 'json',
                    observe: 'response'
                }
            )
            .pipe(
                catchError(errorRes => {
                    return throwError(errorRes);
                })
            );
    }

    fetchGrade(grade: string) {
        return this.http
            .get<any>(
                environment.serverURL + '/grade/' + grade,
                {
                    //   headers: new HttpHeaders({ 'Custom-Header': 'Hello' }),
                    responseType: 'json',
                    observe: 'response'
                }
            )
            .pipe(
                catchError(errorRes => {
                    return throwError(errorRes);
                })
            );
    }

    fetchGrades(grade: string, active: string, year: string, page: string) {
        let searchParams = new HttpParams();
        searchParams = searchParams.append('page', page);
        searchParams = searchParams.append('name', grade);
        searchParams = searchParams.append('active', active);
        searchParams = searchParams.append('year', year);
        return this.http
            .get<any>(
                environment.serverURL + '/grade',
                {
                    //   headers: new HttpHeaders({ 'Custom-Header': 'Hello' }),
                    params: searchParams,
                    responseType: 'json',
                    observe: 'response'
                }
            )
            .pipe(
                catchError(errorRes => {
                    return throwError(errorRes);
                })
            );
    }
}