import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Subject, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { environment } from '../../environments/environment';

interface GradeHomework {
    score: number, annotation: string, homeworkId: number, courseTeacherStudentId: number
}

@Injectable({
    providedIn: 'root'
})
export class HomeworkService {

    constructor(private http: HttpClient) { }

    createAndStoreHomework(id: string, name: string, detail: string, score: number, deliveryDate: string, courseTeacherId: string) {
        const postData = { id: id, name: name, detail: detail, score: score, deliveryDate: deliveryDate, courseTeacherId: courseTeacherId };
        return this.http
            .post(
                environment.serverURL + '/homework/new',
                postData,
                {
                    observe: 'response'
                }
            ).pipe(catchError(errorRes => {
                return throwError(errorRes);
            }));

    }

    fetchHomeworkByCourseId(page: string, courseId: string) {
        let searchParams = new HttpParams();
        searchParams = searchParams.append('course', courseId);
        searchParams = searchParams.append('page', page);
        return this.http
            .get<any>(
                environment.serverURL + '/homework/course-list',
                {
                    params: searchParams,
                    responseType: 'json',
                    observe: 'response'
                }
            )
            .pipe(
                catchError(errorRes => {
                    return throwError(errorRes);
                })
            );
    }

    fetchHomeworkById(homeworkId: string) {
        return this.http
            .get<any>(
                environment.serverURL + '/homework/' + homeworkId,
                {

                    responseType: 'json',
                    observe: 'response'
                }
            )
            .pipe(
                catchError(errorRes => {
                    return throwError(errorRes);
                })
            );
    }

    gradeHomework(gradeHomework: GradeHomework[]) {
        const postData = { checkHomeworkInList: gradeHomework };
        return this.http
            .post(
                environment.serverURL + '/homework/grade',
                postData,
                {
                    observe: 'response'
                }
            ).pipe(catchError(errorRes => {
                return throwError(errorRes);
            }));

    }
}