import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { environment } from '../../environments/environment';

@Injectable({
    providedIn: 'root'
})
export class SectionService {

    constructor(private http: HttpClient) { }

    createAndStoreSection(id: string, name: string, active: boolean) {
        const postData = { id: id, name: name, active: active };
        return this.http
            .post(
                environment.serverURL + '/section',
                postData,
                {
                    observe: 'response'
                }
            ).pipe(catchError(errorRes => {
                return throwError(errorRes);
            }));

    }

    fetchSectionsByGrade(grade: string) {
        return this.http
            .get<any>(
                environment.serverURL + '/grade/gradesections/' + grade,
                {
                    //   headers: new HttpHeaders({ 'Custom-Header': 'Hello' }),
                    responseType: 'json',
                    observe: 'response'
                }
            )
            .pipe(
                catchError(errorRes => {
                    return throwError(errorRes);
                })
            );
    }

    fetchSection(section: string) {
        return this.http
            .get<any>(
                environment.serverURL + '/section/' + section,
                {
                    //   headers: new HttpHeaders({ 'Custom-Header': 'Hello' }),
                    responseType: 'json',
                    observe: 'response'
                }
            )
            .pipe(
                catchError(errorRes => {
                    return throwError(errorRes);
                })
            );
    }

    fetchSectionList() {
        return this.http
            .get<any>(
                environment.serverURL + '/section/list',
                {
                    //   headers: new HttpHeaders({ 'Custom-Header': 'Hello' }),
                    responseType: 'json',
                    observe: 'response'
                }
            )
            .pipe(
                catchError(errorRes => {
                    return throwError(errorRes);
                })
            );
    }

    fetchSections(section: string, active: string, year: string, page: string) {
        let searchParams = new HttpParams();
        searchParams = searchParams.append('page', page);
        searchParams = searchParams.append('name', section);
        searchParams = searchParams.append('active', active);
        searchParams = searchParams.append('year', year);
        return this.http
            .get<any>(
                environment.serverURL + '/section',
                {
                    params: searchParams,
                    responseType: 'json',
                    observe: 'response'
                }
            )
            .pipe(
                catchError(errorRes => {
                    return throwError(errorRes);
                })
            );
    }
}