import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { environment } from '../../environments/environment';

@Injectable({
    providedIn: 'root'
})
export class StudentService {

    constructor(private http: HttpClient) { }

    fetchStudent(firstName: string, lastName: string, gender: string, active: string, year: string, page: string) {
        let searchParams = new HttpParams();
        searchParams = searchParams.append('page', page);
        searchParams = searchParams.append('firstName', firstName);
        searchParams = searchParams.append('lastName', lastName);
        searchParams = searchParams.append('gender', gender);
        searchParams = searchParams.append('active', active);
        searchParams = searchParams.append('year', year);
        return this.http
            .get<any>(
                environment.serverURL + '/student',
                {
                    //   headers: new HttpHeaders({ 'Custom-Header': 'Hello' }),
                    params: searchParams,
                    responseType: 'json',
                    observe: 'response'
                }
            )
            .pipe(
                catchError(errorRes => {
                    return throwError(errorRes);
                })
            );
    }

    createAndStoreStudent(firstName: string, lastName: string, personCharge: string, otherPersonCharge: string, contactPhone1: string, gender: string, dateBirth: string,
        contactEmail: string, isActive: string, contactPhone2: string, contactPhone3: string, allergies: string, gradeAndSection: string) {

        const postData = {
            firstName: firstName, lastName: lastName,
            personCharge: personCharge,
            otherPersonCharge: otherPersonCharge,
            contactPhone1: contactPhone1, gender: gender, dateOfBirth: dateBirth,
            contactEmail: contactEmail, isActive: isActive, contactPhone2: contactPhone2,
            contactPhone3: contactPhone3, allergies: allergies, gradeAndSection: gradeAndSection
        };

        console.log("data " + postData);
        return this.http
            .post(
                environment.serverURL + '/student',
                postData,
                {
                    observe: 'response'
                }
            ).pipe(catchError(errorRes => {
                return throwError(errorRes);
            }));

    }

    fetchStudentsCourse(courseId: string, page: string) {
        let searchParams = new HttpParams();
        searchParams = searchParams.append('page', page);
        searchParams = searchParams.append('course', courseId);
        return this.http
            .get<any>(
                environment.serverURL + '/student/course-list',
                {
                    params: searchParams,
                    responseType: 'json',
                    observe: 'response'
                }
            )
            .pipe(
                catchError(errorRes => {
                    return throwError(errorRes);
                })
            );
    }
}