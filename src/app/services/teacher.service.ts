import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { environment } from '../../environments/environment';
@Injectable({
    providedIn: 'root'
})
export class TeacherService {

    constructor(private http: HttpClient) { }

    fetchTeachers(firstName: string, lastName: string, gender: string, active: string, year: string, page: string) {
        let searchParams = new HttpParams();
        searchParams = searchParams.append('page', page);
        searchParams = searchParams.append('firstName', firstName);
        searchParams = searchParams.append('lastName', lastName);
        searchParams = searchParams.append('gender', gender);
        searchParams = searchParams.append('active', active);
        searchParams = searchParams.append('year', year);
        return this.http
            .get<any>(
                environment.serverURL + '/teacher',
                {
                    //   headers: new HttpHeaders({ 'Custom-Header': 'Hello' }),
                    params: searchParams,
                    responseType: 'json',
                    observe: 'response'
                }
            )
            .pipe(
                catchError(errorRes => {
                    return throwError(errorRes);
                })
            );
    }

    createAndStoreTeacher(firstName: string, lastName: string, contactPhone1: string, gender: string, dateBirth: string,
        contactEmail: string, isActive: string, contactPhone2: string, contactPhone3: string, alergies: string, courses: { gradeId: string, sectionId: string, courseId: string }[]) {

        const postData = {
            firstName: firstName, lastName: lastName, contactPhone1: contactPhone1, gender: gender, dateOfBirth: dateBirth,
            contactEmail: contactEmail, isActive: isActive, contactPhone2: contactPhone2, contactPhone3: contactPhone3, alergies: alergies, courses: courses
        };

        console.log("data " + postData);
        return this.http
            .post(
                environment.serverURL + '/teacher',
                postData,
                {
                    observe: 'response'
                }
            ).pipe(catchError(errorRes => {
                return throwError(errorRes);
            }));

    }


}