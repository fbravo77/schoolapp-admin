import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

const TOKEN_KEY = "AuthToken";
const USERNAME_KEY = "AuthUserName";
const AUTHORITI_KEY = "AuthAuthorities";

@Injectable({
    providedIn: 'root'
})
export class TokenService {

    roles: Array<string> = [];

    constructor() { }

    public setToken(token: string): void {
        window.localStorage.removeItem(TOKEN_KEY);
        window.localStorage.setItem(TOKEN_KEY, token);
    }

    public getToken(): string {
        return window.localStorage.getItem(TOKEN_KEY);
    }

    public setUserName(userName: string): void {
        window.localStorage.removeItem(USERNAME_KEY);
        window.localStorage.setItem(USERNAME_KEY, userName);
    }
    public getUserName(): string {
        return window.localStorage.getItem(USERNAME_KEY);
    }
    public setAuthorities(authorities: string[]): void {
        window.localStorage.removeItem(AUTHORITI_KEY);
        window.localStorage.setItem(AUTHORITI_KEY, JSON.stringify(authorities));
    }
    public getAuthorities(): string[] {
        this.roles = [];
        if (localStorage.getItem(AUTHORITI_KEY)) {
            JSON.parse(localStorage.getItem(AUTHORITI_KEY)).forEach(authority => {
                this.roles.push(authority.authority);
            })
        }
        return this.roles;
    }

    public logOut(): void {
        window.localStorage.clear();
        window.location.reload();
    }


}