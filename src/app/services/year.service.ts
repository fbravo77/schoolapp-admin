import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Subject, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { environment } from '../../environments/environment';
import { Year } from '../models/year';

@Injectable({
  providedIn: 'root'
})
export class YearService {
  error = new Subject<string>();

  constructor(private http: HttpClient) { }

  createAndStoreYear(id: string, year: number, active: boolean) {
    const postData = { id: id, year: year, active: active };
    return this.http
      .post<Year>(
        environment.serverURL + '/year',
        postData,
        {
          observe: 'response'
        }
      ).pipe(catchError(errorRes => {
        return throwError(errorRes);
      }));

  }

  fetchYears(year: string, active: string, page: string) {
    let searchParams = new HttpParams();
    searchParams = searchParams.append('page', page);
    searchParams = searchParams.append('year', year);
    searchParams = searchParams.append('active', active);
    return this.http
      .get<any>(
        environment.serverURL + '/year',
        {
          //   headers: new HttpHeaders({ 'Custom-Header': 'Hello' }),
          params: searchParams,
          responseType: 'json',
          observe: 'response'
        }
      )
      .pipe(
        /* map(responseData => {
           console.log(responseData.body);
           const postsArray: Year[] = [];
           for (const year in responseData.body) {
             console.log("AÑO ", year);
             postsArray.push({ ...responseData[year] });
             /*if (responseData.hasOwnProperty(key)) {
               postsArray.push({ ...responseData[key], id: key });
             }
           }
           return postsArray;
         }),*/
        catchError(errorRes => {
          return throwError(errorRes);
        })
      );
  }
}
