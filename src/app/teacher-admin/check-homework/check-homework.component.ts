import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { faArrowLeft, faSpellCheck } from '@fortawesome/free-solid-svg-icons';
import { HomeworkService } from 'src/app/services/homework.service';
import { StudentService } from 'src/app/services/student.service';
import { ModalComponent } from 'src/app/general/modal/modal.component';
import { MatDialog } from '@angular/material/dialog';

interface GradeHomework {
  score: number, annotation: string, homeworkId: number, courseTeacherStudentId: number
}

@Component({
  selector: 'app-check-homework',
  templateUrl: './check-homework.component.html',
  styleUrls: ['./check-homework.component.scss']
})
export class CheckHomeworkComponent implements OnInit {

  faLeft = faArrowLeft;
  faSpellCheck = faSpellCheck;

  id: string = "";
  courseStudentId: string = "";
  homeworkName: string = "";
  score: string = "";
  deliveryDate: string = "";
  detail: string = "";

  //Student table
  studentData: { id: number, completeName: string, average: string }[] = [];
  displayedStudentColumns: string[] = ['name', 'score', 'annotation'];
  studentResultsLength = 0;
  isLoadingResults = true;

  constructor(private route: ActivatedRoute, private homeworkService: HomeworkService,
    private studentService: StudentService, private location: Location,
    public dialog: MatDialog) { }

  ngOnInit(): void {
    this.getHomework();
    this.fetchStudents();
  }

  getHomework(): void {
    this.id = this.route.snapshot.paramMap.get('homeworkId');
    this.courseStudentId = this.route.snapshot.queryParamMap.get('studentCourse');

    this.homeworkService.fetchHomeworkById(this.id).subscribe(
      homeworkResponse => {
        this.homeworkName = "Tarea: " + homeworkResponse.body.name;
        this.score = homeworkResponse.body.score;
        this.deliveryDate = homeworkResponse.body.deliveryDate;
        this.detail = homeworkResponse.body.detail;
        //        this.fetchStudents();
      },
      error => {
        console.log(error);
      }
    );
  }

  fetchStudents(pageNumber = "0") {
    this.studentService.fetchStudentsCourse(this.courseStudentId, pageNumber).subscribe(
      studentResponse => {
        this.studentData = studentResponse.body.content;
        this.studentResultsLength = studentResponse.body.totalElements;
        this.isLoadingResults = false;
      },
      error => {
        this.isLoadingResults = false;
        this.studentData = [];
        console.log(error);
      }
    );
  }

  goBack(): void {
    this.location.back();
  }



  sendScore() {
    console.log("inicia a calificar");
    let score = document.getElementsByName("points");
    let gradeHomeworkArr: GradeHomework[] = [];
    for (let i = 0; i < score.length; i++) {
      let currentScore = <HTMLInputElement>score[i];
      if (currentScore.value != null && (currentScore).value != "") {
        //Check that the score is not higher than the actual homework score
        if (+currentScore.value > +this.score) {
          this.dialog.open(ModalComponent, { data: { header: "Error", contentModal: "La nota no puede ser mayor a la puntación de la tarea", buttonText: "Ok" } });
          return;
        }
        //get the id
        let id = currentScore.id.split("_")[1];
        //search for the annotation
        let currentAnnotation = <HTMLInputElement>document.getElementById("annotation_" + id)
        let gradeHomework: GradeHomework = { score: +currentScore.value, annotation: currentAnnotation.value, homeworkId: +this.id, courseTeacherStudentId: +id };
        gradeHomeworkArr.push(gradeHomework);
      }
    }

    this.homeworkService.gradeHomework(gradeHomeworkArr).subscribe(
      studentResponse => {
        this.dialog.open(ModalComponent, { data: { header: "Exito", contentModal: "Notas Guardadas con exito", buttonText: "Ok" } });

      },
      error => {
        console.log(error);
      }
    );
    console.log(gradeHomeworkArr);
  }
}
