import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-configuration',
  templateUrl: './configuration.component.html',
  styleUrls: ['./configuration.component.scss']
})
export class ConfigurationComponent implements OnInit {

  cursoCard = { name: "Cursos", imgSrc: "assets/images/course-card-img.png", route: "/admin-teacher/courses", desc: "Ver mis cursos" }
  settingsCard = { name: "Avanzado", imgSrc: "assets/images/settings-card.png", route: "/admin/settings", desc: "Configuraciones Avanzadas" }

  constructor() { }

  ngOnInit(): void {
  }

}
