import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { faArrowLeft, faBars, faEdit, faPlus, faSpellCheck, faTrash, faTrashAlt } from '@fortawesome/free-solid-svg-icons';
import { CourseService } from 'src/app/services/course.service';
import { HomeworkService } from 'src/app/services/homework.service';
import { StudentService } from 'src/app/services/student.service';

@Component({
  selector: 'app-course-detail',
  templateUrl: './course-detail.component.html',
  styleUrls: ['./course-detail.component.scss']
})
export class CourseDetailComponent implements OnInit {
  isLoadingResults = true;

  //Homework table
  homeworkData: { id: number, name: string, deliveryDate: string, score: number, courseTeacherId: number }[] = [];
  displayedHomeworkColumns: string[] = ['name', 'deliveryDate', 'score', 'check'];
  resultsLength = 0;

  //Student table
  studentData: { id: number, completeName: string, average: string }[] = [];
  displayedStudentColumns: string[] = ['id', 'name', 'average'];
  studentResultsLength = 0;

  faLeft = faArrowLeft;
  faPlus = faPlus;
  faBars = faBars;
  faTrash = faTrashAlt;
  faSpellCheck = faSpellCheck;
  faEdit = faEdit;

  id: string = "";
  courseName: string = "";

  totalPoints: number = 0;

  constructor(
    private route: ActivatedRoute,
    private location: Location,
    private courseService: CourseService,
    private homeworkService: HomeworkService,
    private studentService: StudentService) { }

  ngOnInit(): void {
    this.getCourse();
  }

  goBack(): void {
    this.location.back();
  }

  onPageChanged(e) {
    //this.fetchCourses(e.pageIndex.toString())
  }

  fetchHomeworks(pageNumber = "0") {
    this.homeworkService.fetchHomeworkByCourseId(pageNumber, this.id).subscribe(
      homeworkResponse => {
        this.homeworkData = homeworkResponse.body.content;
        this.getTotalPoints();
        this.resultsLength = homeworkResponse.body.totalElements;
        this.isLoadingResults = false;
      },
      error => {
        this.isLoadingResults = false;
        this.homeworkData = [];
        console.log(error);
      }
    );
  }

  fetchStudents(pageNumber = "0") {
    this.studentService.fetchStudentsCourse(this.id, pageNumber).subscribe(
      studentResponse => {
        this.studentData = studentResponse.body.content;

        this.studentResultsLength = studentResponse.body.totalElements;
        this.isLoadingResults = false;
      },
      error => {
        this.isLoadingResults = false;
        this.studentData = [];
        console.log(error);
      }
    );
  }

  getCourse(): void {
    this.id = this.route.snapshot.paramMap.get('id');
    this.courseService.fetchTeacherCourseById(this.id).subscribe(
      coursesResponse => {
        this.courseName = "Curso: " + coursesResponse.body.courseName + " " + coursesResponse.body.gradeName + " " + coursesResponse.body.sectionName;
        this.fetchHomeworks();
        this.fetchStudents();
      },
      error => {
        console.log(error);
      }
    );
  }

  getTotalPoints() {
    this.totalPoints = this.homeworkData.map(t => t.score).reduce((acc, value) => acc + value, 0);
  }
}
