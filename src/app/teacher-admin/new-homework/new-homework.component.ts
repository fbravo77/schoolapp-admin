import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';
import { NgForm } from '@angular/forms';
import { faArrowLeft, faPlus } from '@fortawesome/free-solid-svg-icons';
import { AngularEditorConfig } from '@kolkov/angular-editor';
import { ModalComponent } from 'src/app/general/modal/modal.component';

import { HomeworkService } from 'src/app/services/homework.service';

@Component({
  selector: 'app-new-homework',
  templateUrl: './new-homework.component.html',
  styleUrls: ['./new-homework.component.scss']
})
export class NewHomeworkComponent implements OnInit {

  faLeft = faArrowLeft;
  faPlus = faPlus;

  minDate: Date;
  maxDate: Date;

  id: string = "";
  nameModel: string;
  scoreModel: string;
  deliveryDateModel: string;
  htmlContent: string;

  editorConfig: AngularEditorConfig = {
    editable: true,
    spellcheck: false,
    height: 'auto',
    minHeight: '50',
    placeholder: 'Detalle...',
    sanitize: false
  }

  constructor(private route: ActivatedRoute,
    private homeworkService: HomeworkService,
    public dialog: MatDialog) {
    const currentYear = new Date().getFullYear();
    this.minDate = new Date(currentYear, 0, 1);
    this.maxDate = new Date(currentYear, 12, 31);
  }

  ngOnInit(): void {
    this.id = this.route.snapshot.paramMap.get('courseId');
  }

  onSubmit(form: NgForm) {
    if (form.valid) {
      this.homeworkService.createAndStoreHomework("", form.value.name, this.htmlContent
        , form.value.score, this.deliveryDateModel, this.id).subscribe(
          responseData => {
            if (responseData.status === 200)
              if (this.id !== "" && this.id !== null)
                this.dialog.open(ModalComponent, { data: { header: "Exito", contentModal: "Tarea editado de manera exitosa", buttonText: "Ok" } });
              else
                this.dialog.open(ModalComponent, { data: { header: "Exito", contentModal: "Tarea creado de manera exitosa", buttonText: "Ok" } });
          },
          error => {
            if (error.status === 0) {
              this.dialog.open(ModalComponent, { data: { header: "Error", contentModal: "Servicio no disponible, por favor contacta un administrador", buttonText: "Ok" } });
            }
            else {
              this.dialog.open(ModalComponent, { data: { header: "Error", contentModal: "Operacion fallida, por favor intenta de nuevo o contacta un administrador", buttonText: "Ok" } });
            }
          }
        );
    }
  }
}
