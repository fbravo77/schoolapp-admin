import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { faPlus, faSearch, faArrowLeft, faBars, faFolderOpen } from '@fortawesome/free-solid-svg-icons';
import { CourseService } from 'src/app/services/course.service';
import { GradeService } from '../../services/grade.service';

@Component({
  selector: 'app-teacher-courses',
  templateUrl: './teacher-courses.component.html',
  styleUrls: ['./teacher-courses.component.scss']
})
export class TeacherCoursesComponent implements OnInit {

  courseData: { id: number, name: string, year: string, active: string }[] = [];
  resultsLength = 0;
  isLoadingResults = true;

  faPlus = faPlus;
  faSearch = faSearch;
  faLeft = faArrowLeft;
  faBars = faBars;
  faFolder = faFolderOpen;

  courseModel: string = "";
  yearModel: string = "";
  statusModel = "-1";

  displayedColumns: string[] = ['id', 'course', 'grade', 'section', 'check'];
  dataSource = this.courseData;
  gradeList = [];

  constructor(private gradeService: GradeService, private courseService: CourseService, private location: Location) { }

  ngOnInit(): void {
    this.fetchCourses();
    this.fetchGradesList();
  }

  onPageChanged(e) {
    this.fetchCourses(e.pageIndex.toString())
  }

  fetchCourses(pageNumber = "0") {
    this.courseService.fetchTeacherCourses(this.courseModel, pageNumber).subscribe(
      coursesResponse => {
        this.courseData = coursesResponse.body.content;
        this.resultsLength = coursesResponse.body.totalElements;
        this.isLoadingResults = false;
      },
      error => {
        this.isLoadingResults = false;
        this.courseData = [];
        console.log(error);
      }
    );
  }

  goBack(): void {
    this.location.back();
  }

  fetchGradesList() {
    this.gradeService.fetchGradesList().subscribe(
      gradesResponse => {
        for (let i of gradesResponse.body) {
          this.gradeList.push({ "gradeName": i["gradeName"], "id": i["id"] });
        }
      },
      error => {
        console.log(error);
      }
    );
  }
}
